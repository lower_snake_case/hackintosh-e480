# Hackintoh - ThinkPad E480

## Table of contents
1. Hardware
2. Gathering Kexts
3. Table of references

## Hardware
- CPU/APU: Intel Core i5-8250U (Kaby Lake Rfresh)?
- Memory: 8 GB DDR4 2400 Mhz
- Soundcard: Conexant CX11852
- Networkcard: Realtek PCIe GBE Family Controller (10/100/1000/2500/5000MBit/s), Intel Dual Band Wireless-AC 3165 (a/b/g/n = Wi-Fi 4/ac = Wi-Fi 5), Bluetooth 4.1
- Hard Drives:
    - NVMe: Samsung MZVKV512HAJH-000L1
    - SATA SSD: Samsung SSD 840 EVO 250 GB


## Gathering Kexts

## Table of references
- [OpenCore Desktop Guide](https://dortania.github.io/OpenCore-Desktop-Guide/)
- [Hackintosh Vanilla Desktop Guide](https://hackintosh.gitbook.io/-r-hackintosh-vanilla-desktop-guide/)
- [OpenCore](https://github.com/acidanthera/OpenCorePkg/releases)
- [Lenovo ThinkPad E480 specs](https://www.notebookcheck.com/Lenovo-ThinkPad-E480-20KNCTO1WW.306718.0.html)
